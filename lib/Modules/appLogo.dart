import 'package:flutter/material.dart';
import 'package:clippy_flutter/clippy_flutter.dart';

class AppLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Polygon(
          sides: 7,
          child: Container(
            color: Colors.orange,
            /*decoration: ShapeDecoration(
                color: Colors.orange,
                shape: Border.all(
                  width: 20.0,
                  color: Colors.black,
                )),*/
            alignment: Alignment.center,
            width: 62.5,
            height: 62.5,
            child: Text(
              "A",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 50.0, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Text(
          "rtuseum",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 52.5),
        ),
      ],
    );
  }
}
