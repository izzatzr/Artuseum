import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:async';

class ArtLogin extends StatefulWidget {
  @override
  _ArtLoginState createState() => _ArtLoginState();
}

class _ArtLoginState extends State<ArtLogin> {
  final emailCtrl = new TextEditingController();

  final passCtrl = new TextEditingController();

  final FirebaseAuth _auth = FirebaseAuth.instance;
  var obsec;

  Future<FirebaseUser> _emailAndPassSignIn() async {
    FirebaseUser stats = await _auth.signInWithEmailAndPassword(
        email: emailCtrl.text, password: passCtrl.text);
    print(stats.uid.isNotEmpty);
    return stats;
  }

  @override
  void initState() {
    super.initState();
    obsec = true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.black45,
      ),
      width: 250.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 7.0),
            child: Form(
              //key: formKeya,
              child: TextFormField(
                controller: this.emailCtrl,
                style: TextStyle(color: Colors.white, fontSize: 16.0),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelStyle: TextStyle(color: Colors.orange),
                  labelText: 'Email',
                  hintText: 'email@example.com',
                  hintStyle: TextStyle(color: Colors.white),
                  border: InputBorder.none,
                  suffixIcon: IconButton(
                    onPressed: null,
                    icon: Icon(
                      Icons.email,
                      color: Colors.orange,
                    ),
                  ),
                ),
                //formKeya.currentState.save();
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 7.0),
            child: Form(
              //key: formKeyb,
              child: TextFormField(
                style: TextStyle(color: Colors.white, fontSize: 16.0),
                keyboardType: TextInputType.emailAddress,
                controller: this.passCtrl,
                decoration: InputDecoration(
                  labelStyle: TextStyle(color: Colors.orange),
                  labelText: 'Password',
                  hintStyle: TextStyle(color: Colors.white),
                  hintText: 'your password',
                  border: InputBorder.none,
                  suffixIcon: IconButton(
                    icon: Icon(Icons.visibility),
                    color: Colors.orange,
                    onPressed: () {
                      setState(() {
                        if (obsec) {
                          obsec = false;
                        } else {
                          obsec = true;
                        }
                      });
                    },
                  ),
                ),
                obscureText: obsec,
              ),
            ),
          ),
          IconButton(
            color: Colors.orange,
            alignment: Alignment.center,
            icon: Icon(Icons.check),
            //child: Text(
            //   "Login",
            //),
            onPressed: () => _emailAndPassSignIn().then(
                  (onValue) => onValue.uid.isNotEmpty
                      ? Navigator.pushNamed(context, '/home')
                      : SnackBar(content: Text("Login Error")),
                ),
          ),
        ],
      ),
    );
  }
}

class IncognitoOpt extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.black45,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Text(
            "Incognito",
            style: TextStyle(fontSize: 24.0, color: Colors.orange),
          ),
          Text(
            " for those who just want to visit ",
            style: TextStyle(fontSize: 16.0, color: Colors.white),
          ),
          IconButton(
            onPressed: () => Navigator.pushNamed(context, '/home'),
            iconSize: 48.0,
            icon: Icon(
              Icons.person,
              color: Colors.orange,
            ),
          ),
        ],
      ),
    );
  }
}
