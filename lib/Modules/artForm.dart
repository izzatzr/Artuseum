import 'package:flutter/material.dart';

class ArtForm extends StatefulWidget {
  final List<String> value;
  ArtForm({Key key, this.value}) : super(key: key);

  _ArtFormState createState() => _ArtFormState();
}

class _ArtFormState extends State<ArtForm> {
  Widget artEmail() {
    final emailController = new TextEditingController();
    return Form(
      child: TextFormField(
        decoration:
            InputDecoration(labelText: 'Email', hintText: 'email@example.com'),
        onSaved: (onValue) {
          setState(() {
            super.widget.value.add(emailController.text);
            Form.of(context).save();
          });
        },
      ),
    );
  }

  Widget artPass() {
    final passController = new TextEditingController();
    return Form(
      child: TextFormField(
        decoration: InputDecoration(labelText: 'password', hintText: 'abcdef'),
        onSaved: (onValue) {
          setState(() {
            super.widget.value.add(passController.text);
            Form.of(context).save();
          });
        },
        obscureText: true,
      ),
    );
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    TextEditingController().dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: <Widget>[],
    );
  }

  List<Widget> artCondition() {
    return [
      /*switch (flag) {
      case 'login':
        break;
      case 'register':
        break;
    }*/
    ];
  }
}
