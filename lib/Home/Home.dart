import 'package:flutter/material.dart';
import 'package:clippy_flutter/clippy_flutter.dart';
//import 'package:artuseum/Modules/artAssistance.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'dart:core';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool navBar = true;
  Offset position = Offset(0.0, 0.0);
  var topPos = 0.0;

  Widget button() {
    //BuildContext context;
    return Polygon(
      sides: 6,
      child: FloatingActionButton(
        onPressed: null,
        child: ConstrainedBox(
          constraints: BoxConstraints.expand(),
          child: Image.asset(
            "assets/jpg/1.jpg",
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget assistance() {
    return Positioned(
        left: position.dx,
        top: position.dy,
        child: Draggable(
          childWhenDragging: Container(),
          child: button(),
          onDraggableCanceled: (velocity, offset) {
            setState(() {
              position = offset;
            });
          },
          feedback: button(),
        ));
  }

  Widget top() {
    return AppBar(
      centerTitle: true,
      backgroundColor: Colors.black,
      automaticallyImplyLeading: false,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
              onPressed: () => navTapped(1),
              icon: Icon(
                Icons.person,
                color: Colors.white,
              )),
          IconButton(
              onPressed: () => null,
              icon: Icon(
                Icons.home,
                color: Colors.white,
              )),
          IconButton(
              onPressed: () => navTapped(3),
              icon: Icon(
                Icons.search,
                color: Colors.white,
              )),
        ],
      ),
    );
  }

  void navTapped(int index) {
    //1=profile , 2=home, 3=search
    if (index == 1) Navigator.pushNamed(context, '/profile');
    if (index == 3) Navigator.pushNamed(context, '/search');
  }

  @override
  void initState() {
    super.initState();
    position = Offset(0.0, 0.0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: top(),
      floatingActionButton: SpeedDial(
        elevation: 15.0,
        overlayColor: Colors.transparent,
        overlayOpacity: 0.0,
        backgroundColor: Colors.transparent,
        child: Polygon(
          sides: 7,
          child: ConstrainedBox(
            constraints: BoxConstraints.expand(),
            child: Image.asset(
              "assets/jpg/1.jpg",
              fit: BoxFit.cover,
            ),
          ),
        ),
        children: [
          SpeedDialChild(
              backgroundColor: Colors.orange,
              child: Icon(Icons.add_a_photo),
              onTap: () {}),
          SpeedDialChild(
              child: Icon(Icons.library_music),
              backgroundColor: Colors.orange,
              onTap: () {}),
        ],
      ),
      body: NotificationListener(
        onNotification: (v) {
          if (v is ScrollUpdateNotification)
            setState(() => topPos -= v.scrollDelta / 2);
        },
        child: Stack(
          children: <Widget>[
            ConstrainedBox(
              constraints: BoxConstraints.expand(),
              child: Image.asset(
                "assets/jpg/14.jpg",
                fit: BoxFit.cover,
              ),
            ),
            Column(
              children: <Widget>[
                Expanded(
                  child: Stack(
                    children: <Widget>[
                      CustomScrollView(
                        slivers: <Widget>[
                          SliverGrid(
                            gridDelegate:
                                SliverGridDelegateWithMaxCrossAxisExtent(
                                    mainAxisSpacing: 5.0,
                                    crossAxisSpacing: 5.0,
                                    maxCrossAxisExtent: 350.0),
                            delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                                return Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Card(
                                        color: Colors.transparent,
                                        elevation: 30.0,
                                        child: Container(
                                          height: 250.0,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            image: DecorationImage(
                                                fit: BoxFit.fill,
                                                image: AssetImage(
                                                    "assets/jpg/1.jpg")),
                                          ),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Label(
                                                edge: Edge.BOTTOM,
                                                triangleHeight: 20.0,
                                                child: Container(
                                                  width: 40.0,
                                                  height: 55.5,
                                                  color: Colors.red,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Text(
                                                        "8h",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 15.0),
                                                      ),
                                                      Icon(
                                                        Icons.file_upload,
                                                        color: Colors.white,
                                                        size: 16.0,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Divider(
                                                color: Colors.transparent,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Polygon(
                                                    sides: 7,
                                                    child: Container(
                                                      width: 90.0,
                                                      height: 90.0,
                                                      color: Colors.black45,
                                                      child: Icon(Icons.person),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        //print(filteredNames[index]['name'])
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.star_border,
                                          size: 27.0,
                                        ),
                                        Icon(
                                          Icons.report,
                                          size: 27.0,
                                        )
                                      ],
                                    )
                                  ],
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                      //assistance()
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
