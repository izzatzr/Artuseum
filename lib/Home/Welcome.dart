import 'package:flare_dart/math/mat2d.dart';
import 'package:flutter/material.dart';
import 'package:artuseum/Modules/artSign.dart';
import 'package:artuseum/Modules/appLogo.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flare_flutter/flare_controller.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'dart:core';

//import 'package:google_sign_in/google_sign_in.dart';
//import 'package:simple_auth/simple_auth.dart';
//import 'package:simple_auth_flutter/simple_auth_flutter.dart';

Color artIndicatorA = Colors.orange;
Color artIndicatorB = Colors.black54;

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> implements FlareController {
  ActorAnimation _artActor;
  double _artMount = 0.0;
  double _speed = 1.0;
  double _artTime = 0.0;

  Widget artCarousel() {
    return CarouselSlider(
      viewportFraction: 1.0,
      height: 200.0,
      aspectRatio: 2.0,
      updateCallback: (index) {
        switch (index) {
          case 0:
            setState(() {
              artIndicatorA = Colors.orange;
              artIndicatorB = Colors.black54;
            });
            break;
          case 1:
            setState(() {
              artIndicatorA = Colors.black54;
              artIndicatorB = Colors.orange;
            });
            break;
          default:
        }
      },
      items: [
        ArtLogin(),
        IncognitoOpt(),
        /*artForm(
                              flag: 'login',
                            ),*/
      ],
    );
  }

  Widget dotIndicator() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          width: 8.0,
          height: 8.0,
          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
          decoration:
              BoxDecoration(shape: BoxShape.circle, color: artIndicatorA),
        ),
        Container(
          width: 8.0,
          height: 8.0,
          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
          decoration:
              BoxDecoration(shape: BoxShape.circle, color: artIndicatorB),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          FlareActor(
            "assets/Artuseum.flr",
            animation: "Mirror",
            controller: this,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              AppLogo(),
              artCarousel(),
              dotIndicator(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    onPressed: null,
                    icon: Icon(
                      Zocial.getIconData("facebook"),
                      color: Colors.black54,
                    ),
                  ),
                  IconButton(
                    onPressed: null,
                    icon: Icon(
                      Zocial.getIconData("twitter"),
                      color: Colors.black54,
                    ),
                  ),
                  IconButton(
                    onPressed: null,
                    icon: Icon(
                      Zocial.getIconData("yahoo"),
                      color: Colors.black54,
                    ),
                  ),
                  IconButton(
                    onPressed: null,
                    icon: Icon(
                      Zocial.getIconData("gmail"),
                      color: Colors.black54,
                    ),
                  ),
                  IconButton(
                    onPressed: null,
                    icon: Icon(
                      Zocial.getIconData("instagram"),
                      color: Colors.black54,
                    ),
                  ),
                ],
              ),
            ],
          ),
          //dailyQuote(),
        ],
      ),
    );
  }

  @override
  bool advance(FlutterActorArtboard artboard, double elapsed) {
    _artTime += elapsed * _speed;
    //_artTime = _artActor.duration; //- (_artTime % _artActor.duration);
    _artActor.apply(_artTime, artboard, _artMount);
    return true;
  }

  @override
  void initialize(FlutterActorArtboard artboard) {
    _artActor = artboard.getAnimation("Mirror");
  }

  @override
  void setViewTransform(Mat2D viewTransform) {}
}
