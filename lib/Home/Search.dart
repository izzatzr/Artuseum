import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:clippy_flutter/clippy_flutter.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  // controls the text label we use as a search bar
  final TextEditingController _filter = new TextEditingController();
  final dio = new Dio(); // for http requests
  String _searchText = "";
  List names = new List(); // names we get from API
  List filteredNames = new List(); // names filtered by search text

  _SearchState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredNames = names;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    this._getNames();
    super.initState();
  }

  void _getNames() async {
    final response = await dio.get('https://swapi.co/api/people');
    List tempList = new List();
    for (int i = 0; i < response.data['results'].length; i++) {
      tempList.add(response.data['results'][i]);
    }

    setState(() {
      names = tempList;
      filteredNames = names;
    });
  }

  Widget _buildList(int index) {
    if (_searchText.isEmpty) {
      List tempList = new List();
      for (int i = 0; i < filteredNames.length; i++) {
        if (filteredNames[i]['name']
            .toLowerCase()
            .contains(_searchText.toLowerCase())) {
          tempList.add(filteredNames[i]);
        }
      }
      filteredNames = tempList;
    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 10.0,
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                Image.asset("assets/jpg/1.jpg"),
                Container(
                  child: Text(
                    //filteredNames[index]['name'],
                    "Username",
                    style: TextStyle(color: Colors.black, fontSize: 25.0),
                  ),
                ),
              ],
            ),
            Polygon(
              sides: 7,
              child: Container(
                width: 75.0,
                height: 75.0,
                child: CircleAvatar(
                  backgroundColor: Colors.black38,
                  child: Icon(Icons.person),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          Arc(
            height: 150.0,
            child: Container(
              height: 250.0,
              color: Colors.orange,
            ),
          ),
          CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                floating: true,
                elevation: 0.0,
                automaticallyImplyLeading: false,
                backgroundColor: Colors.transparent,
                title: Container(
                  color: Colors.black38,
                  child: Form(
                    child: TextFormField(
                      cursorColor: Colors.black,
                      controller: _filter,
                      decoration: new InputDecoration(
                        border: InputBorder.none,
                        prefixIcon: new Icon(
                          Icons.search,
                          color: Colors.black,
                        ),
                        hintText: 'Keyword',
                        hintStyle: TextStyle(color: Colors.black),
                      ),
                    ),
                  ),
                ),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) => _buildList(index)),
              ),
            ],
          ),
        ],
      ),
      floatingActionButton: SpeedDial(
        elevation: 15.0,
        overlayColor: Colors.transparent,
        overlayOpacity: 0.0,
        backgroundColor: Colors.transparent,
        child: Polygon(
          sides: 7,
          child: ConstrainedBox(
            constraints: BoxConstraints.expand(),
            child: Image.asset(
              "assets/jpg/1.jpg",
              fit: BoxFit.cover,
            ),
          ),
        ),
        children: [
          SpeedDialChild(
              child: Icon(Icons.home),
              backgroundColor: Colors.orange,
              onTap: () {
                Navigator.pushNamed(context, '/home');
              }),
          SpeedDialChild(
              child: Icon(Icons.person),
              backgroundColor: Colors.orange,
              onTap: () {
                Navigator.pushNamed(context, '/profile');
              }),
        ],
      ),
    );
  }
}
