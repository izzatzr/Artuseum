import 'package:flutter/material.dart';

Widget about() {
  return ListView(
    shrinkWrap: true,
    children: <Widget>[
      ListTile(
        title: Text("Location"),
        subtitle: Text("Indonesia"),
        leading: Icon(Icons.home),
        onTap: null,
      ),
      ListTile(
        title: Text("Sex"),
        subtitle: Text("Male"),
        leading: Icon(Icons.person),
        onTap: null,
      ),
      ListTile(
        title: Text("Occupation"),
        subtitle: Text("Student"),
        leading: Icon(Icons.work),
        onTap: null,
      ),
    ],
  );
}
