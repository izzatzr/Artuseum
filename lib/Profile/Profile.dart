import 'package:flutter/material.dart';
import 'package:artuseum/Modules/gradientColor.dart';
import 'package:clippy_flutter/clippy_flutter.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import 'package:artuseum/Profile/gallery.dart';
import 'package:artuseum/Profile/about.dart';
import 'package:artuseum/Profile/activity.dart';
import 'package:artuseum/Profile/friends.dart';
import 'package:artuseum/Profile/music.dart';
import 'package:artuseum/Profile/writing.dart';

import 'package:firebase_storage/firebase_storage.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> with SingleTickerProviderStateMixin {
  var randcolor = true;
  var appColorA, appColorB, height;
  var choice = SliverFillRemaining(
    child: about(),
  );
  var title;
  TabController tabbar;

  @override
  void initState() {
    super.initState();
    tabbar = TabController(vsync: this, length: 6);
    appColorA = 25.0;
    appColorB = 22.0;
    title = "Profile";
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      height = MediaQuery.of(context).size.height;
    });
    return Scaffold(
      floatingActionButton: SpeedDial(
        elevation: 15.0,
        overlayColor: Colors.transparent,
        overlayOpacity: 0.0,
        backgroundColor: Colors.transparent,
        child: Polygon(
          sides: 7,
          child: ConstrainedBox(
            constraints: BoxConstraints.expand(),
            child: Image.asset(
              "assets/jpg/1.jpg",
              fit: BoxFit.cover,
            ),
          ),
        ),
        children: [
          SpeedDialChild(
              backgroundColor: Colors.orange,
              child: Icon(Icons.photo_album),
              onTap: () {
                setState(() {
                  title = "Photo Gallery";
                  choice = SliverFillRemaining(
                    child: gallery(context),
                  );
                });
              }),
          SpeedDialChild(
              child: Icon(Icons.library_music),
              backgroundColor: Colors.orange,
              onTap: () {
                setState(() {
                  title = "Music Library";
                  choice = SliverFillRemaining(
                    child: music(),
                  );
                });
              }),
          SpeedDialChild(
              child: Icon(Icons.timeline),
              backgroundColor: Colors.orange,
              onTap: () {
                setState(() {
                  title = "Activity";
                  choice = SliverFillRemaining(
                    child: activity(),
                  );
                });
              }),
          SpeedDialChild(
              child: Icon(Icons.speaker),
              backgroundColor: Colors.orange,
              onTap: () {
                setState(() {
                  title = "Writing";
                  choice = SliverFillRemaining(
                    child: writing(),
                  );
                });
              }),
          SpeedDialChild(
              child: Icon(Icons.people),
              backgroundColor: Colors.orange,
              onTap: () {
                setState(() {
                  title = "Friends";
                  choice = SliverFillRemaining(
                    child: friends(),
                  );
                });
              }),
          SpeedDialChild(
              child: Icon(Icons.info),
              backgroundColor: Colors.orange,
              onTap: () {
                setState(() {
                  title = "About";
                  choice = SliverFillRemaining(
                    child: about(),
                  );
                });
              }),
        ],
      ),
      drawer: SizedBox(
        width: 255,
        child: Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [genBackground(), genBackground()])),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.settings,
                        size: 68,
                      ),
                      Text("Settings")
                    ],
                  ),
                ),
              ),
              ListTile(
                title: Text("Change FAB picture"),
                leading: Text("FAB"),
                onTap: null,
              ),
              ListTile(
                title: Text("Change Profile Picture"),
                leading: Icon(Icons.person),
                onTap: null,
              ),
              ListTile(
                title: Text("Change Background"),
                leading: Icon(Icons.image),
                onTap: null,
              ),
              SwitchListTile(
                title: Text('Setting Panel Color'),
                value: randcolor,
                onChanged: (value) {
                  setState(() {
                    randcolor = value;
                  });
                },
                secondary: Icon(Icons.panorama_fish_eye),
              ),
              Slider(
                label: "Theme Color",
                min: 0.00,
                max: 100.00,
                value: appColorA,
                onChanged: (value) {
                  setState(() {
                    appColorA = value;
                  });
                },
              ),
              Slider(
                label: "Theme Color",
                min: 0.00,
                max: 100.00,
                value: appColorB,
                onChanged: (value) {
                  setState(() {
                    appColorB = value;
                  });
                },
              ),
              RaisedButton(
                child: Text("Logout"),
                onPressed: null,
              ),
            ],
          ),
        ),
      ),
      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: <Widget>[
          SliverAppBar(
            floating: true,
            centerTitle: true,
            //title: Text(title),
            backgroundColor: Colors.black,
            expandedHeight: height, //dynamic
            forceElevated: true,
            flexibleSpace: FlexibleSpaceBar(
              background: Stack(
                alignment: Alignment.center,
                fit: StackFit.expand,
                children: <Widget>[
                  Image.asset(
                    "assets/galaxy-1837306_1920.jpg",
                    fit: BoxFit.cover,
                    filterQuality: FilterQuality.high,
                  ),
                  Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Polygon(
                            sides: 7,
                            child: Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: AssetImage("assets/jpg/1.jpg"),
                                ),
                              ),
                              child: RawMaterialButton(
                                onPressed: null,
                                child: null,
                              ),
                              width: 100,
                              height: 100,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(2.0),
                          decoration: BoxDecoration(
                              color: Colors.white30,
                              borderRadius: BorderRadius.circular(8.0)),
                          child: Text(
                            "Izzat Zuliya Rachman",
                            style:
                                TextStyle(color: Colors.white, fontSize: 15.0),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              IconButton(
                padding: EdgeInsets.all(0.0),
                tooltip: "Back",
                icon: Icon(
                  Icons.arrow_forward,
                  color: Colors.white,
                ),
                onPressed: () => Navigator.pop(context),
              ),
            ],
          ),
          choice,
        ],
      ),
    );
  }
}
