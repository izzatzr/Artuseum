import 'package:flutter/material.dart';

Widget friends() {
  return ListView.builder(
    itemBuilder: (BuildContext context, int index) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: <Widget>[
            CircleAvatar(
              maxRadius: 15,
              child: Icon(Icons.person),
            ),
            Padding(
              padding: const EdgeInsets.all(2.0),
              child: Container(
                  decoration: BoxDecoration(
                      color: Colors.black54,
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Text("Friends X")),
            ),
          ],
        ),
      );
    },
  );
}
