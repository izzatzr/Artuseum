import 'package:flutter/material.dart';
import 'package:artuseum/Home/Welcome.dart';
import 'package:artuseum/Home/Home.dart';
import 'package:artuseum/Profile/Profile.dart';
import 'package:flutter/services.dart';
import 'package:artuseum/Home/Search.dart';

void main() {
  runApp(
    MaterialApp(
      title: "Artuseum",
      initialRoute: '/',
      routes: {
        '/': (context) => Welcome(),
        '/home': (context) => Home(),
        '/profile': (context) => Profile(),
        '/search': (context) => Search(),
      },
      debugShowCheckedModeBanner: false,
    ),
  );
  //disable status bar
  SystemChrome.setEnabledSystemUIOverlays([]);
}
